require 'rails_helper'

describe RecordBanner::DummyRecord do
  describe "::to_model_name" do
    subject do
      described_class.to_model_name(arg)
    end

    context "モデル名オブジェクトを渡した場合" do
      let(:arg){ ActiveModel::Name.new(ActiveRecord::Base, nil, "Hoge") }
      it{ is_expected.to equal(arg).and have_attributes(name: "Hoge") }
    end

    context "モデルクラスを渡した場合" do
      let(:arg){ Person }
      it{ is_expected.to equal(arg.model_name).and have_attributes(name: "Person") }
    end

    context "モデルインスタンスを渡した場合" do
      let(:arg){ build(:person) }
      it{ is_expected.to equal(arg.model_name).and have_attributes(name: "Person") }
    end

    context "文字列を渡した場合" do
      context "その名前の定数が存在する場合" do
        context "定数の結果がモデル名メソッドを持つ場合" do
          let(:arg){ "person" }
          it{ is_expected.to be_a(ActiveModel::Name).and have_attributes(name: "Person") }
        end

        context "定数の結果がモデル名メソッドを持たない場合" do
          let(:arg){ "record_banner" }
          it{ is_expected.to be_a(ActiveModel::Name).and have_attributes(name: "RecordBanner") }
        end
      end

      context "その名前の定数が存在しない場合" do
        let(:arg){ "undefined" }
        it{ is_expected.to be_a(ActiveModel::Name).and have_attributes(name: "Undefined") }
      end
    end
  end

  describe "::build" do
    subject do
      described_class.build(arg)
    end

    context "モデル名オブジェクトを渡した場合" do
      let(:arg){ ActiveModel::Name.new(ActiveRecord::Base, nil, "Hoge") }

      it "は、モデル名だけを持つ模擬レコードを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          model_name: equal(arg),
          id: be_nil,
        )
      end
    end

    context "モデルクラスを渡した場合" do
      let(:arg){ Person }

      it "は、モデル名だけを持つ模擬レコードを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          model_name: be_a(ActiveModel::Name).and(have_attributes(name: "Person")),
          id: be_nil,
        )
      end
    end

    context "モデルインスタンスを渡した場合" do
      let(:arg){ build(:person) }

      it "は、モデル名だけを持つ模擬レコードを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          model_name: be_a(ActiveModel::Name).and(have_attributes(name: "Person")),
          id: be_nil,
        )
      end
    end

    context "配列を渡した場合" do
      let(:arg){ [Person, 123] }

      it "は、モデル名とレコードIDを持つ模擬レコードを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          model_name: be_a(ActiveModel::Name).and(have_attributes(name: "Person")),
          id: eq(123),
        )
      end
    end

    context "ハッシュを渡した場合" do
      let(:arg){ {model: Person, id: 123} }

      it "は、モデル名とレコードIDを持つ模擬レコードを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          model_name: be_a(ActiveModel::Name).and(have_attributes(name: "Person")),
          id: eq(123),
        )
      end
    end
  end
end
