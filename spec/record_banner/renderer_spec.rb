require 'rails_helper'

describe RecordBanner::Renderer do
  let(:renderer){ described_class.new(view_context, record) }
  let(:record){ create(:person) }

  describe "#render" do
    subject{ renderer.render }

    it "は、第1引数に渡したレコードのバナー要素を返すこと" do
      is_expected.to have_tag(
        "span.record_banner",
        with: {"data-model" => "Person", "data-record-id" => record.id},
        text: "#<人物 id: #{record.id}>",
      )
    end

    describe "オプション`:href`" do
      context "を渡さない場合 (default)" do
        it "は、レコードの詳細ページへのリンクを含むバナー要素を返すこと" do
          is_expected.to have_tag(".record_banner"){
            with_tag("a", with: {href: "/people/#{record.id}"})
          }
        end
      end

      context "にルートティングが設定されているモデル名のレコードを渡した場合" do
        let(:company){ create(:company) }

        it "は、渡したレコードの詳細ページへのリンクを含むバナー要素を返すこと" do
          expect(renderer.render(href: company)).to have_tag(".record_banner"){
            with_tag("a", with: {href: "/companies/#{company.id}"})
          }
        end
      end

      context "にルートティングが設定されていないモデル名のレコードを渡した場合" do
        let(:animal){ create(:animal) }

        it "は、リンクを含まないバナー要素を返すこと" do
          expect(renderer.render(href: animal)).to have_tag(".record_banner"){
            without_tag("a")
          }
        end
      end

      context "にルートティングが設定されているモデル名の疑似レコードを渡した場合" do
        let(:record){ RecordBanner::DummyRecord.new(Company.model_name, 123) }

        it "は、渡したレコードの詳細ページへのリンクを含むバナー要素を返すこと" do
          expect(renderer.render(href: record)).to have_tag(".record_banner"){
            with_tag("a", with: {href: "/companies/123"})
          }
        end
      end

      context "に未保存のレコードを渡した場合" do
        let(:person){ build(:person) }

        it "は、渡したレコードのモデルの一覧ページへのリンクを含むバナー要素を返すこと" do
          expect(renderer.render(href: person)).to have_tag(".record_banner"){
            with_tag("a", with: {href: "/people"})
          }
        end
      end

      context "にリンク先パスを渡した場合" do
        subject do
          renderer.render(href: "http://example.com/foo/bar")
        end

        it "は、渡したパスへのリンクを含むバナー要素を返すこと" do
          is_expected.to have_tag(".record_banner"){
            with_tag("a", with: {href: "http://example.com/foo/bar"})
          }
        end
      end

      context "にリンク先ルーティング情報を含むハッシュを渡した場合" do
        subject do
          renderer.render(href: {controller: "companies", action: "edit", id: "234"})
        end

        it "は、指定したルーティングへのリンクを含むバナー要素を返すこと" do
          is_expected.to have_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => record.id}){
            with_tag("a", with: {href: "/companies/234/edit"})
          }
        end
      end

      context "に`nil`を渡した場合" do
        it "は、リンクを含まないバナー要素を返すこと" do
          expect(renderer.render(href: nil)).to have_tag(".record_banner"){
            without_tag("a")
          }
        end
      end
    end

    context "HTML属性ハッシュを渡した場合" do
      it "は、渡したHTML属性を持つバナー要素を返すこと" do
        expect(renderer.render(id: "my-id", class: "my-class")).to have_tag("#my-id.record_banner.my-class")
      end
    end

    describe "ブロック" do
      context "を渡さない場合 (default)" do
        it "は、デフォルト内容を持つバナー要素を返すこと" do
          is_expected.to have_tag(".record_banner", text: "#<人物 id: #{record.id}>")
        end
      end

      context "を渡した場合" do
        it "は、ブロック引数にレコードオブジェクトを渡すこと" do
          expect{|block| renderer.render(&block) }.to yield_with_args(equal(record))
        end

        subject do
          renderer.render do |model, id|
            h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
            h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
          end
        end

        it "は、ブロック内で作成した内容を持つバナー要素を返すこと" do
          is_expected.to have_tag(".record_banner"){
            with_tag("a"){
              with_tag("div#shobon", text: "(´･ω･｀)")
              with_tag("span.boon", text: "（＾ω＾）")
            }
          }
        end
      end
    end

    context "レコードが保存済みの場合" do
      it "は、データ属性にレコードのモデル名・レコードIDの情報を持つバナー要素を返すこと" do
        is_expected.to have_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => record.id})
      end
    end

    context "レコードが未保存の場合" do
      let(:record){ build(:person) }

      it "は、データ属性にレコードのモデル名の情報だけを持つバナー要素を返すこと" do
        is_expected.to have_tag(".record_banner:not([data-record-id])", with: {"data-model" => "Person"})
      end
    end
  end

  describe "#default_content" do
    subject{ renderer.default_content }

    context "モデル名の訳文がある場合" do
      it "は、翻訳されたモデル名を含む内容を持つバナー要素を返すこと" do
        is_expected.to eq("#<人物 id: #{record.id}>")
      end
    end

    context "モデル名の訳文が無い場合" do
      let(:record){ create(:animal) }

      it "は、翻訳しないモデル名を含む内容を持つバナー要素を返すこと" do
        is_expected.to eq("#<Animal id: #{record.id}>")
      end
    end

    context "レコードが保存済みの場合" do
      it "は、レコードIDを含む内容を持つバナー要素を返すこと" do
        is_expected.to eq("#<人物 id: #{record.id}>")
      end
    end

    context "レコードが未保存の場合" do
      let(:record){ build(:person) }

      it "は、レコードIDを含まない内容を持つバナー要素を返すこと" do
        is_expected.to eq("#<人物 id: nil>")
      end
    end

    context "レコードIDが未保存の場合" do
      let(:record){ double("Record", model_name: Person.model_name, id: "foobarbaz") }

      it "は、レコードIDを含む内容を持つバナー要素を返すこと" do
        is_expected.to eq('#<人物 id: "foobarbaz">')
      end
    end
  end
end
