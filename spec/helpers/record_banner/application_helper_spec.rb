require 'rails_helper'

describe RecordBanner::ApplicationHelper do
  describe "#record_banner" do
    let(:person){ create(:person) }

    it "は、第1引数に渡したレコードのバナー要素を返すこと" do
      expect(h.record_banner(person)).to have_tag(
        "span.record_banner",
        with: {"data-model" => "Person", "data-record-id" => person.id},
        text: "#<人物 id: #{person.id}>",
      )
    end

    context "第1引数にモデルインスタンスを渡した場合" do
      it "は、ブロック引数に第1引数をそのまま渡すこと" do
        expect{|block| h.record_banner(person, &block) }.to yield_with_args(equal(person))
      end
    end

    context "第1引数に`#model_name`,`#id`を持つオブジェクトを渡した場合" do
      let(:record){ RecordBanner::DummyRecord.new(Person.model_name, 123) }

      it "は、ブロック引数に第1引数をそのまま渡すこと" do
        expect{|block| h.record_banner(record, &block) }.to yield_with_args(equal(record))
      end
    end

    context "第1引数にモデルクラスを渡した場合" do
      it "は、ブロック引数に疑似レコードオブジェクトを渡すこと" do
        expect{|block| h.record_banner(Person, &block) }.to yield_with_args(
          be_a(RecordBanner::DummyRecord).and(have_attributes(model_name: "Person", id: be_nil)),
        )
      end
    end

    context "第1引数にモデルとレコードIDの情報を持つ配列を渡した場合" do
      it "は、ブロック引数に疑似レコードオブジェクトを渡すこと" do
        expect{|block| h.record_banner([Person, 123], &block) }.to yield_with_args(
          be_a(RecordBanner::DummyRecord).and(have_attributes(model_name: "Person", id: 123)),
        )
      end
    end

    context "第1引数にモデルとレコードIDの情報を持つハッシュを渡した場合" do
      it "は、ブロック引数に疑似レコードオブジェクトを渡すこと" do
        expect{|block| h.record_banner({model: Person, id: 123}, &block) }.to yield_with_args(
          be_a(RecordBanner::DummyRecord).and(have_attributes(model_name: "Person", id: 123)),
        )
      end
    end
  end
end
