FactoryBot.define do
  factory :animal do
    sequence(:name){|n| "Animal-#{n}"}
  end
end
