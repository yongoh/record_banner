module RecordBanner
  module ApplicationHelper

    # @param [ActiveRecord::Base,Object] record レコードかそれを表すオブジェクト
    # @return [ActiveSupport::SafeBuffer] レコードのバナー要素
    # @see RecordBanner::Renderer#render
    def record_banner(record, *args, &block)
      record = DummyRecord.build(record) unless record.respond_to?(:model_name) && record.respond_to?(:id)
      Renderer.new(self, record).render(*args, &block)
    end
  end
end
