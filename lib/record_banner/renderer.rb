module RecordBanner
  class Renderer

    # @!attribute [r] record
    #   @return [ActiveRecord::Base,RecordBanner::DummyRecord] レコード
    attr_reader :record

    # @param [ActionView::Base] template ビューコンテキスト
    def initialize(template, record)
      @template, @record = template, record
    end

    # @param [Object] href リンク先パスまたはそれを表すオブジェクト
    # @param [Hash] html_attributes 追加のHTML属性
    # @yield [record] 内容ブロック
    # @yieldparam [ActiveRecord::Base,RecordBanner::DammyRecord] record レコードオブジェクト
    # @return [ActiveSupport::SafeBuffer] レコードのバナー要素
    def render(href: record, **html_attributes)
      content = block_given? ? h.capture{ yield(record) } : default_content
      (content = h.link_to(content, href)) rescue ActionController::UrlGenerationError
      html_attributes.html_attribute_merge!(default_html_attributes)
      h.content_tag(:span, content, html_attributes)
    end

    # @return [String] レコードの概要
    def default_content
      h.translate(
        :format,
        scope: "record_banner",
        model: record.model_name.human,
        id: record.id.inspect,
      )
    end

    # @return [Hash] HTML属性のデフォルト
    def default_html_attributes
      {
        class: "record_banner",
        data: {
          model: record.model_name.name,
          record_id: record.id,
        },
      }
    end

    private

    def h
      @template
    end
  end
end
