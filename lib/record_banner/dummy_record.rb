module RecordBanner
  class DummyRecord
    include ActiveModel::Conversion

    class << self

      # @param [Object] arg モデル名とレコードIDを表すオブジェクト
      # @return [RecordBanner::DummyRecord] 模擬レコードオブジェクト
      def build(arg)
        case arg
        when Hash then build([arg[:model], arg[:id]])
        when Enumerable then new(to_model_name(arg[0]), arg[1])
        else build([arg])
        end
      end

      # @param [Object] arg モデルを表すオブジェクト
      # @return [ActiveModel::Name] モデル名オブジェクト
      def to_model_name(arg)
        if arg.is_a?(ActiveModel::Name)
          arg
        elsif arg.respond_to?(:model_name)
          arg.model_name
        elsif arg.is_a?(Module)
          ActiveModel::Name.new(klass)
        else
          class_name = arg.to_s.classify
          begin
            to_model_name(class_name.constantize)
          rescue NameError
            ActiveModel::Name.new(nil, nil, class_name)
          end
        end
      end
    end

    # @!attribute [r] model_name
    #   @return [ActiveModel::Name] モデル名オブジェクト
    # @!attribute [r] id
    #   @return [Integer,String,nil] レコードID
    attr_reader :model_name, :id

    def initialize(model_name, id)
      @model_name, @id = model_name, id
    end

    # @return [Boolean] 未保存のレコードと同じ扱いであるかどうか
    def new_record?
      !model_name || !id
    end

    # @return [Boolean] 保存済みのレコードと同じ扱いであるかどうか
    def persisted?
      model_name && id
    end
  end
end
